""" Arbitrary length multiplication problem """

from flask import Blueprint, render_template, abort
from config import lti
import hidden_util

from flask_wtf import Form
from wtforms import IntegerField, BooleanField, HiddenField
from jinja2 import TemplateNotFound

from random import randint

problem1 = Blueprint('subject2.problem1', __name__)

class MulForm(Form):
    """ Mul data from Form

    :param Form:
    """
    values = HiddenField('values')
    result = IntegerField('result')
    correct = BooleanField('correct')


@problem1.route('/subject2/problem1', methods=['GET','POST'])
@lti(request='session', app=problem1)
def mul_form(lti=lti):
    """ initial access page for lti consumer

    :param lti: the `lti` object from `pylti`
    :return: index page for lti provider
    """
    form = MulForm()
    values = [randint(1,9) for x in range(2)]
    form.values.data = hidden_util.hidden_pack( values )
    print "pass in values as ", form.values.data
    return render_template('subject2/problem1/mul.html', form=form, values=values, graded=False)


@problem1.route('/subject2/problem1/grade', methods=['POST'])
@lti(request='session', app=problem1)
def grade(lti=lti):
    """ post grade

    :param lti: the `lti` object from `pylti`
    :return: grade rendered by grade.html template
    """
    form = MulForm()
    print "form.values is ", form.values
    print "form.values is ", form.values.data
    vals = hidden_util.hidden_unpack(form.values.data)
    print "vals is ", vals
    product = 1
    for val in vals:
        product *= int(val)
    print "product is ", product
    correct = product == form.result.data

    form.correct.data = correct
    lti.post_grade(1 if correct else 0)
    return render_template('subject2/problem1/mul.html', form=form, values=vals, graded=True, correct=correct)
