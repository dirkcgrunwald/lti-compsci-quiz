""" Simple addition problem """

from flask import Blueprint, render_template, abort
from pylti.flask import lti

from flask_wtf import Form
from wtforms import IntegerField, BooleanField
from jinja2 import TemplateNotFound
from random import randint

problem1 = Blueprint('subject1.problem1', __name__)

class AddForm(Form):
    """ Add data from Form

    :param Form:
    """

    p1 = IntegerField('p1')
    p2 = IntegerField('p2')
    result = IntegerField('result')
    correct = BooleanField('correct')


@problem1.route('/subject1/problem1', methods=['GET','POST'])
#@lti(request='session', error=error, app=app)
def add_form(lti=lti):
    """ initial access page for lti consumer

    :param lti: the `lti` object from `pylti`
    :return: index page for lti provider
    """
    form = AddForm()
    form.p1.data = randint(1, 9)
    form.p2.data = randint(1, 9)
    return render_template('subject1/problem1/add.html', form=form)


@problem1.route('/subject1/problem1/grade', methods=['POST'])
#@lti(request='session', error=error, app=app)
def grade(lti=lti):
    """ post grade

    :param lti: the `lti` object from `pylti`
    :return: grade rendered by grade.html template
    """
    form = AddForm()
    correct = ((form.p1.data + form.p2.data) == form.result.data)
    form.correct.data = correct
#    lti.post_grade(1 if correct else 0)
    return render_template('subject1/problem1/grade.html', form=form)
