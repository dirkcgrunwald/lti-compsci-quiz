import future
import os
from flask import Flask
from flask import Blueprint
from flask import render_template
import jinja2
from jinja2 import ChoiceLoader, FileSystemLoader
from flask_debugtoolbar import DebugToolbarExtension

#from pylti.flask import lti

import sys
import pkgutil
import importlib
import hidden_util
import config


VERSION = '0.0.1'
app = Flask(__name__)
app.config.from_object('config')
app.debug = True
toolbar = DebugToolbarExtension(app)

##
## Set up the LTI problems.
## Problems are contains in subject/problem directory structure.
## Each problem must know the 'subject/problem' path to disambiguate
## template files, and we add the appropriate path here to allow that.
##
##
EXTENSIONS_DIR = "blueprints"
EXTENSIONS_DOC = []

blueprints = pkgutil.iter_modules(path=[EXTENSIONS_DIR])
for subject_loader, subject_name, subject_ispkg in blueprints: 
    if subject_name not in sys.modules:
        ext = importlib.import_module("." + subject_name, EXTENSIONS_DIR)
        problem_modules = pkgutil.iter_modules(path=[EXTENSIONS_DIR + "/" + subject_name])
        for problems_loader, problems_name, problems_spkg in problem_modules:
            full_problem_name = EXTENSIONS_DIR + "." + subject_name + "." + problems_name + "." + problems_name
            subloaded_mod = importlib.import_module(full_problem_name)
            for obj in vars(subloaded_mod).values():
                if isinstance(obj, Blueprint):
                    app.register_blueprint(obj)
                    if getattr(subloaded_mod,'__doc__'):
                        EXTENSIONS_DOC.append([subject_name, problems_name, getattr(subloaded_mod, '__doc__')])


my_loader = jinja2.ChoiceLoader([
        app.jinja_loader,
        jinja2.FileSystemLoader([EXTENSIONS_DIR])
    ])
app.jinja_loader = my_loader


print("URL map is ", app.url_map)
for rule in app.url_map.iter_rules():
    print(rule)

def error(exception=None):
    """ render error page

    :param exception: optional exception
    :return: the error.html template rendered
    """
    return render_template('error.html')

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET'])
@app.route('/lti/', methods=['GET', 'POST'])
@app.route('/is_up', methods=['GET'])
@config.lti(request='initial', error=error, app=app)
def default(lti=config.lti):
    """ Indicate the app is working. Provided for debugging purposes.

    :param lti: the `lti` object from `pylti`
    :return: simple page that indicates the request was processed by the lti
        provider
    """
    urls = [ ["/" + l[0] + "/" + l[1], l[2]] for l in EXTENSIONS_DOC ]
    return render_template('up.html', lti=lti, extensions=urls)

def set_debugging():
    """ enable debug logging

    """
    import logging
    import sys

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)

set_debugging()

if __name__ == '__main__':
    """
    For if you want to run the flask development server
    directly
    """
    port = int(os.environ.get("FLASK_LTI_PORT", 5000))
    host = os.environ.get("FLASK_LTI_HOST", "localhost")
    app.run(debug=True, host=host, port=port)
