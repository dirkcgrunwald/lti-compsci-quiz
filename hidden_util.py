import json
import config
import hmac
import base64
import future

##
## Simple module to 

def hidden_pack(x):
    payload = json.dumps(x)
    hash = hmac.new(config.SECRET_KEY, payload).digest()
    result = { "payload" : payload,
               "hash" : base64.b64encode(hash)
               }
    return json.dumps(result)

def hidden_unpack(x):
    try:
        unpacked = json.loads(x)
        if len(unpacked) == 2 and 'payload' in unpacked and 'hash' in unpacked:
            payload = unpacked['payload']
            myhash = base64.b64encode( hmac.new(config.SECRET_KEY, payload).digest() )
            if myhash == unpacked['hash']:
                return json.loads(payload)
        return None
    except:
        return None

if __name__ == '__main__':
    print( "hi")
    data = [0, 2, 4]
    print("data is ", data)
    hidden = hidden_pack(data)
    print("hidden_pack is ", hidden)
    unhidden = hidden_unpack(hidden)
    print("hidden_unpack is ", unhidden)
